require 'yaml'

desc "drop the HeadSpringDirectory database"
task :drop_database  do 

  yml = ensure_dev_yml_exists

  drop = <<-SCRIPT

    IF DB_ID('HeadSpringDirectory') IS NOT NULL 
      BEGIN 
        ALTER DATABASE [HeadSpringDirectory] 
          SET SINGLE_USER WITH ROLLBACK IMMEDIATE;

        DROP DATABASE [HeadSpringDirectory]; 
      END
    SCRIPT

  puts "Dropping HeadSpringDirectory database"

  sh "SqlCmd -E -S #{ yml["database"] } -d master -Q \"#{ drop }\""
end

desc "creates, seeds and adds simulation tables to the HeadSpringDirectory database"
task :create_database  do 

  yml = ensure_dev_yml_exists

  puts "Creating the base HeadSpringDirectory database"

  create = "HeadSpringDirectoryScript.sql"
  seed = "Seeddata.sql"
 
  sh "SqlCmd -E -S #{ yml["database"] } -d master -i \"#{ create }\", \"#{ seed}\""

  create_user = <<-SCRIPT

    CREATE USER [HeadSpringUser] FOR LOGIN [HeadSpringUser]
    GO

    EXEC sp_addrolemember 'db_owner', 'HeadSpringUser'

    SCRIPT

  puts "Creating user HeadSpringUser user in the HeadSpringDirectory database"

  sh "SqlCmd -E -S #{ yml["database"] } -d HeadSpringDirectory -Q \"#{ create_user }\""
end

desc 'creates an initial dev.yml file' 
task :init_dev_yml do
  File.open("dev.yml", 'w') { |f| f.write("database: (local)") }
end

def ensure_dev_yml_exists

  if !File.exists? "dev.yml"
    raise <<-MSG
      It doesn't look like dev.yml exists on this machine.  Run the init_dev_yml rake task to create the dev.yml.  
      After the file is created, you'll want to inspect the file and change the default values.
      MSG
  end

  yml = YAML::load File.open("dev.yml")

  return yml

end
