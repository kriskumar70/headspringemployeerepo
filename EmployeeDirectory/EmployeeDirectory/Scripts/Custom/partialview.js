﻿$(document).ready(function () {

    
    $(".mainTableCell").click(function () {
        
        if ($(this).children.length > 0 && $(this).children(0)[0] != null
            && $(this).children(0)[0].outerHTML.indexOf("<a ") > -1)
            return;

        var url = $(this).parent().attr('title');
        if(url!=null)
            window.location = url;

    });

    //$(".selectedRow").click(function () {

    //    var x = $(this).text();

    //    var url = $(this).attr('title');
    //    window.location = url;
    //});

    $('.clickComments').on('click', function (event) {
       
        var notes = $(this).attr("title");
        if (notes == null || notes == "")
            return;
        $(this).addClass("selected").append('<div class="messagepop pop">' + notes + '<p><a class="close" href="/">close</a></p></div>');
        $(".pop").slideFadeToggle();
        return false;
    });
       

    $(".close").on('click', function () {
        $(".pop").slideFadeToggle();
        $(".messagepop").remove();
        $(".clickNotes").removeClass("selected");
        $(".pop").toggle();
        return false;
    });

    $.fn.slideFadeToggle = function (easing, callback) {
        return this.animate({ opacity: 'toggle', height: 'toggle' }, "fast", easing, callback);
    };

  
    $(".fileInput").on("change", function (e) {

        var url = $(this).attr('title');
        var formData = new FormData(); //$('#EditPersonForm').data;
        var file = document.getElementById("fileUpload").files[0];
        if (file == null)
            return;
        formData.append("FileUpload", file);
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                window.location.reload(false);
            },
            error: function (error) {

            }
        });
        
    });


    $(".deleteItem").on("click", function (e) {
        e.preventDefault();
        
        if ($(this).parent().length > 0) {
            if ($(this).parent()[0].className == "listItem")
                var lstItem = $(this).parent()[0];
        }
        var url = $(this).attr('href');

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            success: function (data) {
                if (data.ErrMsg != null)
                    alert(data.ErrMsg);
                else
                {
                    if (lstItem!=null)
                        $(lstItem).remove();
                    else
                        window.location.reload(false);
                }
                    
            },
            error: function (error) {
                alert(error.responseText());
            }
        });
    })
    
    $(".actionLink").on("click", function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var formName = $(this).attr("id");
        $("#dialog-edit").dialog({
            title: 'Sync Project',
            autoOpen: false,
            resizable: true,
            height: 'auto',
            width: 'auto', //550,
            //show: 'blind',
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: true,
            dialogClass: 'ui-dialog-osx',
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
                $(this).load(url);

            },
            close: function (event, ui) {
                //$("#dialog-edit").html("");
                $(this).dialog("close");

              // $("#dialog-edit").dialog("close");
            },
            buttons: [
                    {
                        text: "Save",
                        "class": 'btn btn-default',
                        click: function () {
                            var file = document.getElementById("projectFile").files[0];
                            if (file != null) {

                                $(this).dialog("close");
                                uploadFile(url);
                            }
                            else
                                window.location.reload(false);
                        }
                    },
                    {
                        text: "Cancel",
                        "class": 'btn btn-default',
                        click: function() {
                            $("#dialog-edit").dialog("close");
                        }
                    }
            ]

        });

        $("#dialog-edit").dialog('open');
        return false;
    });

    

    //Person functions
    function uploadFile(uploadUrl) {
        var formData = new FormData(); //$('#EditPersonForm').data;
        var file = document.getElementById("projectFile").files[0];
        if (file == null)
            return;
        formData.append("projectFile", file);
        $.ajax({
            type: "POST",
            url: uploadUrl,
            //url: "/File/UploadImageFile?personId=" + personId,
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: showProgressDialog,
            complete: hideProgressDialog,
            success: function (response) {
                if (response.ErrMsg != null)
                    alert(response.ErrMsg);
                else
                    window.location.reload(false);
            },
            error: function (error) {

            }
        });
    };

    function showProgressDialog() {

        $("#progressDialog").dialog({
            autoOpen: false,
            modal: true,
            bgiframe: true
        });

        $('#progressDialog').dialog('open');
        $(".ui-dialog-titlebar-close").hide();
    };

    function hideProgressDialog() {

        if ($('#progressDialog').dialog('isOpen')) {

            $('#progressDialog').dialog('close');
        }
    };
  
});
