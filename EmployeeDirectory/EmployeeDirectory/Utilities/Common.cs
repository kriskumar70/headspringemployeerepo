﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using EmployeeDirectory.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EmployeeDirectory.Utilities
{
    public class Common
    {
        //public static List<SelectListItem> GetStatuses(Enum status)
        //{
        //    SelectListItem item;
        //    List<SelectListItem> Statuses = new List<SelectListItem>();
        //    var Status = Enum.GetValues(typeof(Status));// db.Status.OrderBy(s => s.Description).ToList();

        //    foreach (var r in Status)
        //    {
        //        item = new SelectListItem();
        //        item.Text = ((Enum)r).Description();
        //        item.Value = ((Enum)r).Value<int>().ToString();
        //        if (status != null && ((Enum)r) == status)
        //        {
        //            item.Selected = true;
        //        }
        //        else
        //        {
        //            item.Selected = false;
        //        }
        //        Statuses.Add(item);
        //    }


        //    return Statuses;
        //}

        //public static SelectList GetRolesSelectList()
        //{
        //    var roleSelectList = new GenericRepository<Role>(new EPLResearchContext()).GetAll().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            
        //    return new SelectList(roleSelectList, "Value", "Text");
        //}

        public static string GetMd5Hash(string value)
        {
            var md5Hasher = MD5.Create();
            var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            var sBuilder = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        //public static AllProUser GetCurrentUser(string userName)
        //{
        //    UserManager<AllProUser> userManager = new UserManager<AllProUser>(new UserStore<AllProUser>(new ApplicationDbContext()));
        //    return userManager.FindByName(HttpContext.Current.User.Identity.Name);
        //}
        //public static List<AllProUser> GetAllUsers()
        //{
        //    UserManager<AllProUser> userManager = new UserManager<AllProUser>(new UserStore<AllProUser>(new ApplicationDbContext()));
        //    return userManager.Users.ToList();
        //}
        public static Guid GetCurrentUserId()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var user =  userManager.FindByName(HttpContext.Current.User.Identity.Name);
            return Guid.Parse(user.Id);

        }
      
        //public static AllProUser GetUserById(Guid userId)
        //{
        //    UserManager<AllProUser> userManager = new UserManager<AllProUser>(new UserStore<AllProUser>(new ApplicationDbContext()));
        //    var user = userManager.FindById(userId.ToString());
        //    return user;

        //}
    }
}