﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EmployeeDirectory.Utilities
{
    public static class Extensions
    {
        public static bool IsAdmin(this WebViewPage page)
        {
            return page.User.IsInRole("HRUser");
        }
        public static bool IsAuthorized(this WebViewPage page)
        {
            return page.User.IsInRole("HRUser") || page.User.IsInRole("ApplicationUser");
        }
        public static MvcHtmlString If(this MvcHtmlString value, bool evaluation)
        {
            return evaluation ? value : MvcHtmlString.Empty;
        }

        //public static string Description(this Enum value)
        //{
        //    return GetDescription(value);
        //}

        //public static string Name(this Enum value)
        //{
        //    return Enum.GetName(value.GetType(), value);
        //}

        //public static T Value<T>(this Enum value)
        //{
        //    return (T)Enum.ToObject(value.GetType(), value);
        //}

   

        //public static Employee Employee(this AllProUser user)
        //{
        //    var employeeRepository = new GenericRepository<Employee>(new AllProDbContext());
        //    return employeeRepository.GetById(user.EmployeeId);

        //}

        //public static AllProUser CreatedByUser(this IDomainModel model)
        //{
        //    UserManager<AllProUser> userManager = new UserManager<AllProUser>(new UserStore<AllProUser>(new ApplicationDbContext()));
        //    var user = userManager.FindById(model.CreatedBy.ToString());
        //    return user;
            
        //}

        //public static AllProUser ModifiedByUser(this IDomainModel model)
        //{
        //    UserManager<AllProUser> userManager = new UserManager<AllProUser>(new UserStore<AllProUser>(new ApplicationDbContext()));
        //    var user = userManager.FindById(model.ModifiedBy.ToString());
        //    return user;
            
        //}
    }
}