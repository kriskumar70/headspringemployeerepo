﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using EmployeeDirectory.DAL;
using EmployeeDirectory.Models;

namespace EmployeeDirectory.Controllers
{
    public class EmployeeApiController : ApiController
    {
        //
        [HttpGet]
        public IEnumerable<Employee> GeteEmployees(string query = "")
        {
            using (var db = new EmployeeContext())
            {
                return String.IsNullOrEmpty(query) ? db.Employees.ToList() :
                db.Employees.Where(p => p.FirstName.Contains(query)).ToList();
            }
        }
    }
}