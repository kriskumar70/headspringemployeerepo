﻿using System.Net;
using System.Web.Mvc;

namespace EmployeeDirectory.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return View();
        }

        public ActionResult InternalServerError()
        {
            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return View();
        }

    }
}
