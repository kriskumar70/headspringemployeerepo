﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmployeeDirectory.Utilities;
using PagedList;
using EmployeeDirectory.Models;
using EmployeeDirectory.DAL;

namespace EmployeeDirectory.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeContext context = new EmployeeContext();
        private EmployeeRepository employeeRepository = new EmployeeRepository();
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: /Employee/

         [Authorize]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.LastNameSortParm = String.IsNullOrEmpty(sortOrder) ? "last_name" : "";
            ViewBag.FirstNameSortParm = String.IsNullOrEmpty(sortOrder) ? "first_name" : "";
            //ViewBag.FirsttNameSortParm = sortOrder == "FirstName" ? "first_name" : "FirstName";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            var employees = context.Employees.AsQueryable();

            if (!String.IsNullOrEmpty(searchString))
            {
                employees = employeeRepository.SelectEmployees(searchString);
                //employees = employees.Where(s => s.FirstName.Contains(searchString)
                //    || s.LastName.Contains(searchString));
           }
            switch (sortOrder)
            {
                case "last_name":
                    employees = employees.OrderBy(s => s.LastName);
                    break;
                case "first_name":
                    employees = employees.OrderBy(s => s.FirstName);
                    break;
                default:  // Name ascending 
                    employees = employees.OrderBy(s => s.LastName);
                    break;
            }
           

            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(employees.ToPagedList(pageNumber, pageSize));

            //return View(db.Employees.ToList());
        }

        // GET: /Employee/Details/5
         [Authorize(Roles = "ApplicationUser")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = context.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: /Employee/Create
       [Authorize]
        public ActionResult Create()
        {
            //Get list of states..
            var states = UnitedStates.States;
            var titles = context.Titles.ToList().Select(c => new SelectListItem
            {
                Value = c.id.ToString(),
                Text = c.JobTitle
            });

            var model = new Employee();
            model.States = states;
            model.Titles = titles;
            return View(model);
        }

        // POST: /Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,LastName,FirstName,MiddleName,TitleId,Address1,Address2,City,State,Zip,Email,PhoneWork,PhoneCell")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    employee.CreatedBy = (Guid)Common.GetCurrentUserId();
                    employee.CreatedOn = DateTime.Now;
                    context.Employees.Add(employee);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message); 
                    //logger.Info(ex.Message);
                }
              
            }

            return View(employee);
        }

        // GET: /Employee/Edit/5
          [Authorize(Roles = "HRUser")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = context.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            //Get list of states..
            var states = UnitedStates.States;
            var titles = context.Titles.ToList().Select(c => new SelectListItem
            {
                Value = c.id.ToString(),
                Text = c.JobTitle
            });

         
            ViewBag.State = states;
            ViewBag.Titles = titles;
            var model = new Employee();
            employee.States = states;
            employee.Titles = titles;
            return View(employee);
        }

        // POST: /Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
          public ActionResult Edit([Bind(Include = "Id,LastName,FirstName,MiddleName,TitleId,Address1,Address2,City,State,Zip,Email,PhoneWork,PhoneCell,CreatedBy,CreatedOn")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.ModifiedBy = (Guid)Common.GetCurrentUserId();
                employee.ModifiedOn = DateTime.Now;
                
                Employee emp = context.Employees.Find(employee.Id);
                employee.CreatedOn = emp.CreatedOn;
                employee.CreatedBy = emp.CreatedBy;
                ((IObjectContextAdapter)context).ObjectContext.Detach(emp);
                context.Entry(employee).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: /Employee/Delete/5
          [Authorize(Roles = "HRUser")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = context.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: /Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = context.Employees.Find(id);
            context.Employees.Remove(employee);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
