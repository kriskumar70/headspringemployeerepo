﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using EmployeeDirectory.Models;

namespace EmployeeDirectory.DAL
{
    public class EmployeeRepository : BaseRepository
    {
        public IQueryable<Employee> SelectEmployees(string searchString)
        {
            using (IDbConnection connection = OpenConnection())
            {
                string query = "SELECT Id,LastName,FirstName,MiddleName,TitleId,Address1,Address2,City,State,Zip,Email,PhoneWork,PhoneCell,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn " +
                                     "FROM dbo.Employee e " +
                                     "where FREETEXT(FirstName, '" +
                                       searchString +"')";
                return connection.Query<Employee>(query).AsQueryable();
            }
        }

      
    }
}