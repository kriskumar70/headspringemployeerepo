﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmployeeDirectory.Models
{
       [Table("Title")]
    public class Title
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(150)]
        public string JobTitle { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}