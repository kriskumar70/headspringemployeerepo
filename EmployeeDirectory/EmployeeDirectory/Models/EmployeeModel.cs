﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Web.Mvc;
using EmployeeDirectory.DAL;

namespace EmployeeDirectory.Models
{
    [Table("Employee")]
    public partial class Employee
    {
        public int Id { get; set; }
        [Display(Name = "Last Name")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [Display(Name = "Address")]
        [StringLength(50)]
        public string Address1 { get; set; }

        [StringLength(50)]
        public string Address2 { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(5)]
        public string State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [Display(Name = "Primary Email")]
        [StringLength(50)]
        public string Email { get; set; }

       [Display(Name = "Business #")]
        [StringLength(30)]
        public string PhoneWork { get; set; }

       [Display(Name = "Mobile #")]
        [StringLength(30)]
        public string PhoneCell { get; set; }

        //public int? StatusId { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public Guid? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? TitleId { get; set; }
        // This property will hold all available states for selection
        public IEnumerable<SelectListItem> States { get; set; }

        public virtual Title Title { get; set; }
        // This property will hold all available Titles for selection
        public IEnumerable<SelectListItem> Titles { get; set; }
    }
}
