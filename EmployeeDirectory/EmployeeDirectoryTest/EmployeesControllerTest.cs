﻿using System;
using System.Web.Mvc;
using EmployeeDirectoryTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EmployeeDirectory.Controllers;
namespace EmployeeDirectoryTest
{
    [TestClass]
    public class EmployeesControllerTest : IntegrationTestBase
    {
        [TestMethod]
        public void Edit_Positive_Get_Test()
        {
            // Arrange
            var controller = DependencyHelper.GetInstance<EmployeeController>();

            // Act
         
             var viewResult = controller.Details(3076) as ViewResult;

            // Assert
            Assert.AreEqual(string.Empty, viewResult.ViewName);
        }
    }
}
